let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var backendPlugin = 'resources/assets/back-end/plugins/';
var frontendPlugin = 'resources/assets/front-end/plugins/';

mix.js('resources/js/back-end/app.js', 'public/js/back-end/app.js')
    .combine([
        backendPlugin + 'js/core/jquery.min.js',
        backendPlugin + 'js/core/popper.min.js',
        backendPlugin + 'js/core/bootstrap.min.js',
        backendPlugin + 'js/plugins/perfect-scrollbar.jquery.min.js',
        backendPlugin + 'js/plugins/chartjs.min.js',
        backendPlugin + 'js/plugins/bootstrap-notify.js',
        backendPlugin + 'js/now-ui-dashboard.min.js?v=1.2.0',
        backendPlugin + 'demo/demo.js',
        'public/js/back-end/app.js',
    ], 'public/js/back-end/bundle.min.js')
    .browserSync('laravue');
