<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('assets/img/favicon.png')}}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="url" content="{{ url('/') }}" />
    <title>
        Weekend Tooltle
    </title>
    
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css">
    <!-- CSS Files -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/now-ui-dashboard.css?v=1.2.0')}}" rel="stylesheet" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.css">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" />
    <style>
        .content .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: 0px;
            margin-left: 0px;
        }
        .vdpArrowPrev:after {
            border-right-color: #cc99cd;
        }

        .vdpArrowNext:after {
            border-left-color: #cc99cd;
        }

        .vdpCell.selectable:hover .vdpCellContent,
        .vdpCell.selected .vdpCellContent {
            background: #cc99cd;
        }

        .vdpCell.today {
            color: #cc99cd;
        }

        .vdpTimeUnit > input:hover,
        .vdpTimeUnit > input:focus {
            border-bottom-color: #cc99cd;
        }
        .vdpComponent.vdpWithInput>input {
            font-size: 16px;
            display: block;
            width: 100%;
            box-sizing: border-box;
            padding: 10px;
            padding-right: 60px;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 4px;
            background: #0000;
            border: 1px solid #e0e0e0;
            box-shadow: 0 0.1em 0.3em rgba(0,0,0,.05);
            outline: 0;
        }
        .vdpComponent.in-valid.vdpWithInput>input{
            border-color: red;
        }
        .sidebar, .off-canvas-sidebar {
            position: fixed;
            top: 0;
            height: 100%;
            bottom: 0;
            width: 260px;
            left: 0;
            z-index: 5;
        }
        .dropdown-menu {
            border: 0 !important;
        }
        ul li .dropdown-menu {
            border: 0 !important;
            border-top: 0 !important;
        }
        .navbar a:not(.btn):not(.dropdown-item) {
            color: #888;
        }
        .nav-item.dropdown{
            margin-left: 0.5em;
        }
        .vdpOuterWrap.vdpFloating {
            z-index: 6 !important;
        }
        @media (max-width: 576px){
            li.nav-item.dropdown a {
                color: #888 !important;
            }
        }
        .modal{
            z-index: 1050 !important;
        }
    </style>

</head>

<body class="sidebar-mini">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div id="root">
        <loading v-if="$root.loading"></loading>
        <router-view></router-view>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUq0NZJC7fr79Ky2q0DED1iTxE0IRKmXU&libraries=places" async
      defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/1.2.1/lodash.min.js"></script>
    <script src="{{asset('js/back-end/bundle.min.js')}}"></script>

</body>
</html>