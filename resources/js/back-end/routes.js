import VueRouter from "vue-router";
import helper from "./services/helper";
let routes = [{
        path: "/",
        component: require("./layouts/master"),
        meta: {
            requiresAuth: true
        },
        children: [{
                path: "/",
                component: require("./views/home")
            },
            {
                path: "/dashboard",
                component: require("./views/home")
            },
            {
                path: "/medias",
                component: require("./views/medias/index")
            },
            {
                path: "/categories",
                component: require("./views/categories/index")
            },
            {
                path: "/categories/create",
                component: require("./views/categories/create")
            },
            {
                path: "/categories/:id/edit",
                component: require("./views/categories/create")
            },
            {
                path: "/events",
                component: require("./views/events/index")
            },
            {
                path: "/hotels",
                component: require("./views/hotels/index")
            },
            {
                path: "/restaurants",
                component: require("./views/restaurants/index")
            },
            {
                path: "/restaurant/:restaurantId/list",
                component: require("./views/restaurants/list")
            },
            {
                path: "/restaurant/:restaurantId/add",
                component: require("./views/restaurants/create")
            },
            {
                path: "/restID/:restaurantId/restaurant/:id/edit",
                component: require("./views/restaurants/create")
            },
            {
                path: "/hotel/:hotelId/list",
                component: require("./views/hotels/list")
            },
            {
                path: "/hotel/:hotelId/add",
                component: require("./views/hotels/create")
            },
            {
                path: "/hotelID/:hotelId/hotel/:id/edit",
                component: require("./views/hotels/create")
            },
            {
                path: "/posts",
                component: require("./views/posts/index")
            },
            {
                path: "/posts/create",
                component: require("./views/posts/create")
            },
            {
                path: "/posts/:id/edit",
                component: require("./views/posts/create")
            },
            {
                path: '/users/create',
                component: require('./views/user/create')
            },
            {
                path: '/users',
                component: require('./views/user/index')
            },
            {
                path: '/users/:id/edit',
                component: require('./views/user/create')
            },
            {
                path: '/role/create',
                component: require('./views/role/create')
            },
            {
                path: '/roles',
                component: require('./views/role/index')
            },
            {
                path: '/role/:id/edit',
                component: require('./views/role/create')
            },
            {
                path: '/users/profile',
                component: require('./views/commons/profile')
            },
            {
                path: '/sliders',
                component: require('./views/sliders/index')
            },
            {
                path: '/sliders/:id/edit',
                component: require('./views/sliders/create')
            },
            {
                path: '/slider/create',
                component: require('./views/sliders/create')
            },
        ]
    },
    {
        path: "/",
        component: require("./layouts/auth/master"),
        meta: {
            requiresGuest: true
        },
        children: [{
                path: "/login",
                component: require("./views/auth/login")
            },

            {
                path: "/register",
                component: require("./views/auth/register"),
                meta: {
                    windowRedirectAfter: true
                }
            },
            {
                path: "/forget-password",
                component: require("./views/auth/forget-password")
            },
            {
                path: "/auth/social",
                component: require("./views/auth/social")
            }
        ]
    },
    {
        path: "*",
        component: require("./layouts/error-page"),
        children: [{
            path: "*",
            component: require("./views/errors/page-not-found")
        }]
    }
];

const router = new VueRouter({
    /*linkActiveClass: 'active',
    mode: 'history',*/
    routes
});

router.beforeEach((to, from, next) => {
    router.app.loading = true;
    let noIntentUrl = ['/login','/register','/forget-password','/auth/social'];
    if(noIntentUrl.indexOf(to.fullPath) == -1){
        router.app.$store.dispatch('setIntended',to.fullPath);
    }
    
    if (to.matched.some(m => m.meta.requiresAuth)) {
        return helper.check().then((response) => {
            if (!response) {
                router.app.loading = false;
                return next({
                    path: "/login"
                });
            }
            return next();
        });
    }

    if (to.matched.some(m => m.meta.requiresGuest)) {
        return next();
    }
});
router.afterEach((to, from, next) => {
    setTimeout(() => (router.app.loading = false), 1000);
});

export default router;
