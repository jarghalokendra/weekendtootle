import { Toast } from 'buefy/dist/components/toast'
export default {
    data(){
        return {
            baseUrl : this.$store.getters.getBaseUrl,
        }
    },

    logout(){
        return axios.post('/api/auth/logout').then(response =>  {
            localStorage.removeItem('auth_token');
            axios.defaults.headers.common['Authorization'] = null;
            toastr['success'](response.data.message);
        }).catch(error => {
            console.log(error);
        });
    },

    authUser(){
        return axios.get('/api/auth/user').then(response =>  {
            return response.data.authenticated;
        }).catch(error => {
            return error.response.data;
        });
    },

    check(){
        return axios.post('/api/auth/check').then((response) =>  {
            return response.data.authenticated;
        }).catch((error) =>{
            return false;
        });
    },

    formAssign(form, data){
        for (let key of Object.keys(form)) {
            if(key !== "originalData" && key !== "errors" && key !== "autoReset"){
                form[key] = data[key];
            }
        }
        return form;
    },

    formatDate(date){
        if(!date)
            return;

        return moment(date).format('MMMM Do YYYY');
    },

    formatDateTime(date){
        if(!date)
            return;

        return moment(date).format('MMMM Do YYYY h:mm a');
    },

    ucword(value){
        if(!value)
            return;

        return value.toLowerCase().replace(/\b[a-z]/g, function(value) {
            return value.toUpperCase();
        });
    },

    ToastMessage(data){
        Toast.open({
            duration: 4000,
            message: data.message,
            position: 'is-bottom-right',
            type: data.type
        });
    },

    getDate(date = null) {
        var d = new Date();
        if (date !== null) {
            d = new Date(date);
        }
        var currentDate =
            d.getFullYear().toString() +
            "-" +
            ((d.getMonth() + 1).toString().length == 2 ?
                (d.getMonth() + 1).toString() :
                "0" + (d.getMonth() + 1).toString()) +
            "-" +
            (d.getDate().toString().length == 2 ?
                d.getDate().toString() :
                "0" + d.getDate().toString()) +
            " " +
            (d.getHours().toString().length == 2 ?
                d.getHours().toString() :
                "0" + d.getHours().toString()) +
            ":" +
            ((parseInt(d.getMinutes() / 5) * 5).toString().length == 2 ?
                (parseInt(d.getMinutes() / 5) * 5).toString() :
                "0" + (parseInt(d.getMinutes() / 5) * 5).toString());
        return currentDate;
    },

    removeFirstletter(word){
        return word.slice(1);
    },

    setSource(d){
        localStorage.setItem('image-source',d);
    },

    removeSource(){
        localStorage.removeItem('image-source');
    },

    getSource(){
        return localStorage.getItem('image-source');
    }
}
