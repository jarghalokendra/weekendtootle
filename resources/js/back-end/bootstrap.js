import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import 'vue-multiselect/dist/vue-multiselect.min.css';
import Helper from './services/helper.js';
import VeeValidate from 'vee-validate';
import Multiselect from 'vue-multiselect';
import firebase from 'firebase';
import VuePlaceAutocomplete from 'vue-place-autocomplete';

let config = {
    apiKey: "AIzaSyA6g9pTVaEwukDugUFvHBgN6fJYfWNLSLU",
    authDomain: "sbari44648.firebaseapp.com",
    databaseURL: "https://sbari44648.firebaseio.com",
    projectId: "sbari44648",
    storageBucket: "sbari44648.appspot.com",
    messagingSenderId: "672657567629",
    appId: "1:672657567629:web:f9c9eff7e32cf78c"
};

window.Vue = Vue;
Vue.use(VueRouter);
Vue.use(VuePlaceAutocomplete);
Vue.use(Buefy);
Vue.use(VeeValidate, {errorBagName: 'bErrors'});
window.axios = axios;
window.VuePlaceAutocomplete = VuePlaceAutocomplete;
Vue.component('multiselect', Multiselect);

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
let baseUrl= document.head.querySelector('meta[name="url"]').content;
window.axios.defaults.baseURL = baseUrl;

firebase.initializeApp(config);
let db = firebase.database();
window.firebase=firebase;
window.db = db;


axios.interceptors.request.use(
    (config) => {
        let token = localStorage.getItem('access_token')
        if (token) {
          config.headers['Authorization'] = `Bearer ${ token }`
        }
        return config
    },
    (error) => {
        return Promise.reject(error)
    }
)
/**
* response handlar 
* Mostly intended to handle 401 error
*/
axios.interceptors.response
    .use(function (response) {
        if(response.status==200){
            if(typeof response.data.message !== 'undefined')
                Helper.ToastMessage({
                    message: response.data.message,
                    type: 'is-info'
                });
        }
    	return response;
  	}, function (error) {
    if(error.response.data.status=='unauthorised' && error.response.status==401){
        Helper.ToastMessage({
            message: 'Unauthorised access level.',
            type: 'is-danger'
        });
    }
    else if(error.response.status==401 && error.response.statusText=="Unauthorized"){
        Helper.ToastMessage({
            message: 'Oops..! Session has expired!',
            type: 'is-danger'
        });
        this.$store.dispatch('reSetVuex');
    	this.$router.push('/login');
    }
    return Promise.reject(error);
});

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

// let token = document.head.querySelector('meta[name="csrf-token"]');

// if (token) {
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
// } else {
//     console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
// }

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
