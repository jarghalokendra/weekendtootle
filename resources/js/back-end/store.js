import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		url :{
			baseUrl: document.head.querySelector('meta[name="url"]').content
        },
        profile: {
			data: {}
        },
        permissions: {
            data: []
        },
        intended: {
            url:''
        },
        notifications: [],
        firebaseRef: {
            key: ''
        }
	},
	mutations: {
		setBaseUrl(state,url){
			state.url.baseUrl = url.baseUrl
        },
        setProfile(state,profile){
            for(var prop in profile){
                //state.profile.data[prop]=profile[prop];
                Vue.set(state.profile.data, prop, profile[prop])
            }
        },
        setPermissions(state,permissions){
            if(permissions!==undefined && permissions.length>0){
                permissions.forEach(function(permission){
                    state.permissions.data.push(permission);
                })
            }
        },
        setNotifications(state,data){
            if(data){
                data.forEach(function(item){
                    state.notifications.data.push({'id': item.id, message: item.message});
                })
            }
        },
        setFireBaseRef(state,key){
            if(key){
                state.firebaseRef.key = key;
            }
        },
        
        reSetVuex(state){
            state.profile.data={};
            state.permissions.data=[];
            state.notifications.data=[];
        },
        setIntended(state,url){
            state.intended.url=url;
        },
        resetIntended(state){
            state.intended.url = '';
        }
	},
	actions: {
		setProfile({commit},data){
            commit('setProfile',data);
        },
        setPermissions({commit},data){
            commit('setPermissions',data);
        },
        setNotifications({commit},data){
            commit('setNotifications',data);
        },
        setIntended({commit},url){
            commit('setIntended',url);
        },
        resetIntended({commit}){
            commit('resetIntended');
        },
        reSetVuex({commit}){
            commit('reSetVuex');
        }

	},
	getters: {
		getBaseUrl: (state) => {
		    return state.url['baseUrl'];
        },
        getProfile: (state) =>{
            return state.profile.data;
        },
        getPermissions: (state)=>{
            return state.permissions.data;
        },
        getIntended: (state)=>{
            return state.intended.url
        },
        getNotifications: (state)=>{
            return state.notifications
        },
        getFireBaseRef: (state)=>{
            return state.firebaseRef.key;
        }
    },
    plugins: [createPersistedState()],
});

export default store;