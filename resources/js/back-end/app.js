require('./bootstrap');
import store from './store'
import router from './routes'
import loading from './layouts/loading'

const app = new Vue({
    el: '#root',
    data: {
        loading: true
    },
    components: {
        loading: loading
    },
    router,
    store
});
