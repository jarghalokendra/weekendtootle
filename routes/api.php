<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('/login', 'LoginController@authenticate');
    Route::post('/logout', 'LoginController@logout');
    Route::post('/check', 'LoginController@check');
    Route::post('/register', 'RegisterController@register');
    Route::post('/social/token', 'SocialAuthController@getSocialToken');
    Route::post('/social/{provider}', 'LoginController@social');
});

Route::group(['namespace' => 'Backend', 'middleware'=>'auth:api'],function(){
	Route::post('/common/menus','CommonController@menus');
	Route::get('/common/categories','CommonController@categories');
	Route::get('/common/tags','CommonController@tags');
    Route::get('/common/posts','CommonController@posts');
	Route::get('/common/roles','CommonController@roles');
	Route::get('/common/places','CommonController@getPlaces');
	Route::get('/common/profile','CommonController@getProfile');
    Route::post('/common/common-module','CommonController@getCommonModules');
    Route::post('/common/update-profile', 'CommonController@updateProfile');
});

Route::group(['namespace' => 'Backend\Media', 'middleware' => 'auth:api'], function () {
    Route::post('media/upload', 'MediaController@upload');
    Route::get('media', 'MediaController@index');
    Route::post('media/delete', 'MediaController@delete');
});

Route::group(['prefix' => 'dashboard', 'namespace' => 'Backend\Dashboard', 'middleware' => 'auth:api'], function () {
    Route::get('/', 'DashboardController@index');
});

Route::group(['namespace' => 'Backend\Post', 'middleware' => 'auth:api'], function () {
    Route::get('/posts', 'PostController@index');
    Route::post('/posts/create', 'PostController@store');
    Route::post('/posts/status', 'PostController@status');
    Route::post('/posts/delete', 'PostController@delete');
    Route::get('/posts/{id}/edit', 'PostController@edit');
    Route::post('/posts/update/{id}', 'PostController@update');
    Route::post('/events/update/{id}', 'EventController@update');
});
Route::group(['namespace' => 'Backend\Sliders', 'middleware' => 'auth:api'], function () {
    Route::get('/sliders', 'SliderController@index');
    Route::post('/slider/create', 'SliderController@store');
    Route::post('/sliders/status', 'SliderController@status');
    Route::post('/sliders/delete', 'SliderController@delete');
    Route::get('/slider/{id}/edit', 'SliderController@edit');
    Route::post('/slider/update/{id}', 'SliderController@update');
});

Route::group(['namespace' => 'Backend\Restaurant', 'middleware' => 'auth:api'], function () {
    Route::get('/restaurants/{id}', 'RestaurantController@index');
    Route::post('/restaurant/create', 'RestaurantController@store');
    Route::post('/restaurant/status', 'RestaurantController@status');
    Route::post('/restaurant/delete', 'RestaurantController@delete');
    Route::get('/restaurant/{id}/edit', 'RestaurantController@edit');
    Route::post('/restaurant/update/{id}', 'RestaurantController@update');
});

Route::group(['namespace' => 'Backend\Hotel', 'middleware' => 'auth:api'], function () {
    Route::get('/hotels/{id}', 'HotelController@index');
    Route::post('/hotel/create', 'HotelController@store');
    Route::post('/hotel/status', 'HotelController@status');
    Route::post('/hotel/delete', 'HotelController@delete');
    Route::get('/hotel/{id}/edit', 'HotelController@edit');
    Route::post('/hotel/update/{id}', 'HotelController@update');
});

Route::group(['namespace'=>'Backend\Role', 'middleware'=>'auth:api'],function(){
	Route::post('/role/create', 'RoleController@store');
	Route::get('/role/{id}/edit', 'RoleController@edit');
	Route::post('/role/update/{id}', 'RoleController@update');
	Route::post('/roles/delete', 'RoleController@delete');
	Route::post('/roles/status', 'RoleController@status');
	Route::get('/roles', 'RoleController@index');
});

Route::group(['namespace'=>'Backend\Category', 'middleware'=>'auth:api'],function(){
	Route::post('/categories/create', 'CategoryController@store');
	Route::get('/categories/{id}/edit', 'CategoryController@edit');
	Route::post('/categories/update/{id}', 'CategoryController@update');
	Route::post('categories/delete', 'CategoryController@delete');
	Route::post('categories/status', 'CategoryController@status');
	Route::get('/categories', 'CategoryController@index');
});

Route::group(['namespace'=>'Backend\User', 'middleware'=>'auth:api'],function(){
	Route::post('/users/create', 'UserController@store');
	Route::get('/users/{id}/edit', 'UserController@edit');
	Route::post('/users/update/{id}', 'UserController@update');
	Route::post('/users/status', 'UserController@status');
	Route::post('/users/delete', 'UserController@delete');
	Route::get('/users', 'UserController@index');
});


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::get('/data/slider-ad', 'SliderAdController@getSliders');
    Route::get('/data/feature-ad', 'FeatureAdController@getFeatureAd');
    Route::post('/data/near-by-ad', 'NearByAdController@nearBy');
    Route::post('/data/ads', 'AdController@ads');
    Route::post('/data/ads/type/ad-post-id', 'AdController@getAdsByPostId');
    Route::post('/data/ads/type/ad-post-slug', 'AdController@getAdsByPostSlug');
    Route::post('/data/ad/type/single-ad-id', 'AdItemController@getSingleAdById');
    Route::post('/data/ad/type/single-ad-slug', 'AdItemController@getSingleAdBySlug');
    Route::group(['middleware'=>'auth:api'], function(){

    });
});
