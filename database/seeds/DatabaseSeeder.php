<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
<<<<<<< HEAD
    	$this->call([
            UsersTableSeeder::class,
            RolesTableSeeder::class,
            PostsTableSeeder::class,
            CountriesTableSeeder::class,
            CitiesTableSeeder::class,
        ]);
=======
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('posts')->truncate();
        for ($i = 0; $i <= 50; $i++) {
            \DB::table('posts')->insert([
                'post_title' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' . $i,
                'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et diam lorem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas porta finibus lectus quis venenatis. Curabitur id fermentum neque, ornare malesuada magna. Praesent sit amet nisi vitae arcu congue porta. Ut euismod, nulla vel auctor pharetra, quam mi vulputate metus, nec tempor est nunc et odio. Aenean ligula leo, fermentum et imperdiet nec, ullamcorper ac mi.' . $i,
                'post_excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et diam lorem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas porta finibus lectus quis venenatis. Curabitur id fermentum neque, ornare malesuada magna.' . $i,
                'slug' => 'Lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit' . $i,
                'feature_image' => $i <= 10 ? 2 : 3,
                'post_type' => 'post',
                'user_id' => 2,
                'post_status' => $i <= 10 ? 0 : 1,
                'city_id' => 1,
                'updated_by' => 1,
                'latitude' => '',
                'longitude' => '',
            ]);
        }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
>>>>>>> shyam
    }

	
}