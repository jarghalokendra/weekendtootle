<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('roles')->insert($this->getRoles());
    }

    public function getRoles()
    {
    	return [
            ['name'=>'admin', 'description'=>'admins', 'created_at'=> date("Y-m-d h:m:s"),
            'updated_at'=> date("Y-m-d h:m:s") ],
            ['name'=>'vendor', 'description'=>'vendors', 'created_at'=> date("Y-m-d h:m:s"),
            'updated_at'=> date("Y-m-d h:m:s") ],
            ['name'=>'user', 'description'=>'users', 'created_at'=> date("Y-m-d h:m:s"),
            'updated_at'=> date("Y-m-d h:m:s") ],
        ];
    }
}
