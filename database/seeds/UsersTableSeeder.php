<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->insert([
            'first_name' => 'admin',
            'last_name' => 'Backend',
            'email' => 'admin@backend.com',
            'status' => '1',
            'ip_address' => '',
            'last_login' => date("Y-m-d h:m:s"),
            'password' => bcrypt('secret'),
            'city_id' =>'1',
            'country_id' =>'1',
            'created_at'=> date("Y-m-d h:m:s"),
            'updated_at'=> date("Y-m-d h:m:s")
        ]);
        DB::table('role_user')->insert([
            'role_id' => '1',
            'user_id' => '1'
        ]);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
