<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $faker = Faker::create();
    	DB::table('cities')->insert([
            'name' =>  $faker->city,
            'country_id' => '1',
            'district'=> $faker->name,
            'zone' => $faker->city,
            'state' => $faker->state,
        ]);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
