<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\Services\CommonServices;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class NotificationServices
{

    public function __construct(CommonServices $commonServices)
    {
        $this->commonServices = $commonServices;
        $this->serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/sbari44648-firebase-adminsdk-uybbf-efabac1fcd.json');
    }

    public function saveNotifications($data){
        $adata = $data;
        $roles = ['admin'];
        if(!empty($data['roles'])){
            $roles = $data['roles'];
        }
        foreach ($roles as $val) {
            $data['to'] = $this->commonServices->getRoleID($val);
            $notification = \App\Models\Notification::create($data);
        }
    }

    public function pushNotifications($data){
        $firebase = (new Factory)
        ->withServiceAccount($this->serviceAccount)
        ->withDatabaseUri('https://sbari44648.firebaseio.com')
        ->create();
        $database = $firebase->getDatabase();
        $newPost = $database
        ->getReference('notifications')
        ->push($data);
        return $newPost->getKey(); // => -KVr5eu8gcTv7_AHb-3-
        //$newPost->getUri(); // => https://my-project.firebaseio.com/blog/posts/-KVr5eu8gcTv7_AHb-3-

        //$newPost->getChild('title')->set('Changed post title');
        //$newPost->getValue(); // Fetches the data from the realtime database
        //$newPost->remove();
    }

}