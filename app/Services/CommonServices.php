<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

class CommonServices
{

    public function __construct()
    {

    }

    public function getRoles()
    {
        $roles = [];
        $authId = null;
        if (auth()->user()) {
            $authId = auth()->user()->id;
        } else {
            return $roles;
        }

        $user = \App\Models\User::select('id')
            ->with('roles')
            ->where(['id' => $authId])
            ->first();
        if (!empty($user)) {
            foreach ($user->roles as $role) {
                $roles[] = $role->name;
            }
        }
        return $roles;
    }

    public function isAdmin()
    {
        if (in_array('admin', $this->getRoles())) {
            return true;
        }
        return false;
    }

    public function isVendor()
    {
        if (in_array('vendor', $this->getRoles())) {
            return true;
        }
        return false;
    }

    public function isUser()
    {
        if (in_array('user', $this->getRoles())) {
            return true;
        }
        return false;
    }

    public function getMenuByRoles()
    {
        $fmenus = [];
        $menus = getMenus();
        foreach ($menus as $key => $menu) {
            foreach ($this->getRoles() as $role) {
                if (in_array($role, $menu['permissions'])) {
                    $fmenus[] = $menu;
                }
            }
        }
        $fmenus = array_unique($fmenus, SORT_REGULAR);
        return $fmenus;
    }
    
    public function categories(){
        return \App\Models\Category::where('status',1)->orderBy('name','asc')->get();
    }

    public function tags(){
        return \App\Models\Tag::where('status',1)->orderBy('name','asc')->get();
    }
    
    public function roles(){
        return \App\Models\Role::where('status',1)->orderBy('name','asc')->get();
    }

    public function posts(){
        return \App\Models\Post::where('post_status',1)->orderBy('post_title','asc')->get();
    }

    public function getDefaultRole(){
        $role = \App\Models\Role::where('name','vendor')->first();
        if(!empty($role)){
            $id = $role->id;
        }
        else{
            $role = \App\Models\Role::create([
                'name'=>'vendor'
            ]);
            $id = $role->id;
        }

        return $id;
    }

    public function getRoleID($role){
        $d = \App\Models\User::whereHas('roles',function($q) use($role){
            $q->where(['name'=> $role]);
        })->first();
        return $d->id;
    }

    public function getIdsArray($items=[]){
        $ids=[];
        foreach ($items as $item) {
            $ids[] = $item['id'];
        }
        return $ids;
    }

    public function getTagsId($tags){
        foreach ($tags as $tag) {
            if(\App\Models\Tag::where('name', $tag['name'])->count() == 0){
                $tagM =new \App\Models\Tag([
                    'name' => $tag['name']
                ]);
                $tagM->save();
                $ids[] = $tagM->id;
            }
            if(!empty($tag['id']))
            $ids[]= $tag['id'];
        }
        return $ids;
    }

    public function getProfile(){
        return \App\Models\User::where('id',auth()->user()->id)->with('profileimage')->first();
    }

    public function getCommonModule(){
        return [
            'permissions' => $this->getRoles(),
            'profile'   => $this->getProfile()
        ];
    }

}