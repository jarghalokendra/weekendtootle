<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all()['post_type'];
        return [
            'post_title' => 'required',
            'post_excerpt'=> 'required',
            'post_type'=> 'required',
            'location.longitude'=> 'required',
            'phone'=> 'required',
            'email'=> 'email',
            'location.latitude'=> 'required',
            'start_date'=> $data && $data['value']=='event' ? 'required' : '',
            'end_date'=> $data && $data['value']=='event' ? 'required' : '',
        ];
    }
}