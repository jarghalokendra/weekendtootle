<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'phone_no'=> 'required',
            'mobile_no'=> 'required',
            'address'=> 'required',
            'email' => 'sometimes|required|email|unique:users,email,'.$this->id,
            'password' => 'sometimes|required|min:8|confirmed',
            'password_confirmation' => 'sometimes|required|min:8'
        ];
    }
}