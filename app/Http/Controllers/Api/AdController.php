<?php

namespace App\Http\Controllers\Api;
use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class AdController extends Controller
{
    /**
    * @SWG\Post(
    *     path="/v1/data/ads",
    *     tags={"Ads"},
    *     summary="Post Ads are listed here based on the type/or none eg: event or none param",
    *     
    *     @SWG\Parameter(
    *         name="type",
    *         in="query",
    *         schema={},
    *         type="string",
    *         description="Type"
    *     ),
    *     @SWG\Response(
    *          response=200,
    *          description="Item list",
    *      ),
    *     @SWG\Response(
    *          response="default",
    *          description="error"
    *   )
    * ),
    */

    public function ads(Request $request){
        $paginate = 20;
        if(!$request->has('type')){
            $items = \App\Models\Post::with(['postfile', 'tags', 'categories','createdby','files'])
            ->paginate($paginate);
        }
        else {
            $items = \App\Models\Post::where(['post_type'=>$request->type])
            ->with(['postfile','tags', 'categories','createdby','files'])
            ->paginate($paginate);
        }
        return response()->json(['items'=>$items, 'metas'=>['version'=> env('APP_VERSION')]], 200);
    }

    /**
    * @SWG\Post(
    *     path="/v1/data/ads/type/ad-post-id",
    *     tags={"Ads"},
    *     summary="Post children Ads are listed here based on the Id and type eg: 123/hotel",
    *     
    *     @SWG\Parameter(
    *         name="Post_Id",
    *         in="query",
    *         schema={},
    *         type="string",
    *         required= true,
    *         description="Post_Id"
    *     ),
    *     @SWG\Parameter(
    *         name="type",
    *         in="query",
    *         schema={},
    *         type="string",
    *         required= true,
    *         description="Type"
    *     ),
    *     @SWG\Response(
    *          response=200,
    *          description="Ads list",
    *      ),
    *     @SWG\Response(
    *          response="default",
    *          description="error"
    *   )
    * ),
    */

    public function getAdsByPostId(Request $request){
        $paginate = 20;
        $items = [];
        $profile = \App\Models\Post::with(['postfile','tags'])->find($request->Post_Id);
        if($request->type == 'hotel'){
            $items = \App\Models\Hotel::with(['featureImage', 'tags','createdby'])
            ->where(['post_id'=>$request->Post_Id])->paginate($paginate);
        }
        elseif($request->type == 'restaurant'){
            $items = \App\Models\Restaurant::with(['featureImage', 'tags','createdby'])
            ->where(['post_id'=>$request->Post_Id])->paginate($paginate);
        }
        return response()->json(['items'=>$items, 'metas'=>['version'=> env('APP_VERSION'), 'profile' => $profile]], 200);
    }

    /**
    * @SWG\Post(
    *     path="/v1/data/ads/type/ad-post-slug",
    *     tags={"Ads"},
    *     summary="Post Children Ads are listed here based on the slug and type eg: abc-event/hotel (we can use it in case of optional of Post_Id and can use in web rather than in app)",
    *     
    *     @SWG\Parameter(
    *         name="post_slug",
    *         in="query",
    *         schema={},
    *         required= true,
    *         type="string",
    *         description="Post Slug"
    *     ),
    *     @SWG\Parameter(
    *         name="type",
    *         in="query",
    *         schema={},
    *         required= true,
    *         type="string",
    *         description="Type"
    *     ),
    *     @SWG\Response(
    *          response=200,
    *          description="Post children Ads are listed here based on the slug and type eg: abc-event/restaurant",
    *      ),
    *     @SWG\Response(
    *          response="default",
    *          description="error"
    *   )
    * ),
    */
    public function getAdsByPostSlug(Request $request){
        $paginate = 20;
        $items = [];
        $profile = \App\Models\Post::with(['postfile','tags'])->where(['slug'=>$request->post_slug])->first();
        if($request->type == 'hotel'){
            $items = \App\Models\Hotel::with(['featureImage', 'tags','createdby','files'])
            ->where(['post_id'=>$profile->id])->paginate($paginate);
        }
        elseif($request->type == 'restaurant'){
            $items = \App\Models\Restaurant::with(['featureImage', 'tags','createdby', 'files'])
            ->where(['post_id'=>$profile->id])->paginate($paginate);
        }
        return response()->json(['items'=>$items, 'metas'=>['version'=> env('APP_VERSION'), 'profile' => $profile]], 200);

    }
}