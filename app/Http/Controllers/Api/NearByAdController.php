<?php

namespace App\Http\Controllers\Api;
use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

/**
* 	@SWG\post(
*     	path="/v1/data/near-by-ad",
*     	tags={"NearBYAd"},
*     	summary="near by data are fetches here, based on lat and lang",
*     	@SWG\Parameter(
* 			name="latitude",
* 			in="query",
*          	schema={},
* 			required=true,
* 			type="integer",
* 			description="latitude",
*		),
*		@SWG\Parameter(
*			name="longitude",
*			in="query",
*			schema={},
*			required=true,
*			type="integer",
*			description="longitude"

* 		),
*     	@SWG\Response(
*          response=200,
*          description="near by data are fetches here, based on lat and lang"
*      	),
*     	@SWG\Response(
*			response="default",
*			description="error"
*		)
* 	),
*/
class NearByAdController extends Controller
{
    public function nearBy(Request $request)
    {
    	$coordinates = [
    		'latitude'=> $request->latitude,
    		'longitude'=> $request->longitude
    	];
    	$radius = 400;
    	$data = Post::where('post_status',1)
        ->with(['postfile'])
    	->isWithinMaxDistance($coordinates, $radius)
    	->get();
    	$fdata= [
    		'event' => [],
    		'hotel' => [],
    		'restaurant' => []
    	];
    	if(!empty($data)){
    		foreach ($data as $key => $item) {
    			foreach ($fdata as $k => $d) {
    				if($k==$item->post_type)
    				$fdata[$k][] = $item;
    			}
    		}
    	}
    	return response()->json(['data'=>$fdata, 'metas'=>['app_version'=>env('APP_VERSION')]]);
    }
}