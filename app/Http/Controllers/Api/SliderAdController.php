<?php

namespace App\Http\Controllers\Api;
use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Http\Request;


class SliderAdController extends Controller
{

    /**
    * @SWG\Get(
    *     path="/v1/data/slider-ad",
    *     tags={"Sliders"},
    *     summary="Slider Ad data are found here!",
    *     @SWG\Response(
    *          response=200,
    *          description="sliders data",
    *      ),
    *     @SWG\Response(
    *          response="default",
    *          description="error"
    *   )
    * ),
    */

    public function getSliders(){
    	$sliders = \App\Models\Slider::where(['status'=>1, 'type'=>'slider'])
        ->with(['post'=>function($q){
            return $q->with('postfile');
        }])
        ->orderBy('cost','desc')
        ->get();
        return response()->json(['sliders'=>$sliders, 'metas'=> ['app_version'=> env('APP_VERSION')]], 200);
    }
}