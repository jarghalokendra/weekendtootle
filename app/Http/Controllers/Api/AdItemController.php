<?php

namespace App\Http\Controllers\Api;
use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class AdItemController extends Controller
{
    /**
    * @SWG\Post(
    *     path="/v1/data/ad/type/single-ad-id",
    *     tags={"Ads"},
    *     summary="Ad detail info is listed here based on the Id and type eg: 123/hotel",
    *     
    *     @SWG\Parameter(
    *         name="Id",
    *         in="query",
    *         schema={},
    *         type="string",
    *         required= true,
    *         description="Id"
    *     ),
    *     @SWG\Parameter(
    *         name="type",
    *         in="query",
    *         schema={},
    *         type="string",
    *         required= true,
    *         description="Type"
    *     ),
    *     @SWG\Response(
    *          response=200,
    *          description="Ads list",
    *      ),
    *     @SWG\Response(
    *          response="default",
    *          description="error"
    *   )
    * ),
    */

    public function getSingleAdById(Request $request){
        if($request->type == 'event'){
            $item = \App\Models\Post::with(['postfile', 'tags','files','categories','createdby'])
            ->find($request->Id);
        }
        elseif($request->type == 'hotel'){
            $item = \App\Models\Hotel::with(['tags', 'featureImage', 'files','createdby'])
            ->find($request->Id);
        }
        elseif($request->type == 'restaurant'){
            $item = \App\Models\Restaurant::with(['featureImage','tags', 'files','createdby'])
            ->find($request->Id);
        }
        
        return response()->json(['item'=>$item, 'metas'=>['version'=> env('APP_VERSION')]], 200);

    }

    /**
    * @SWG\Post(
    *     path="/v1/data/ad/type/single-ad-slug",
    *     tags={"Ads"},
    *     summary="Ad detail info is listed here based on the slug and type eg: hotel/abc-t",
    *     
    *     @SWG\Parameter(
    *         name="slug",
    *         in="query",
    *         schema={},
    *         type="string",
    *         required= true,
    *         description="slug"
    *     ),
    *     @SWG\Parameter(
    *         name="type",
    *         in="query",
    *         schema={},
    *         type="string",
    *         required= true,
    *         description="Type"
    *     ),
    *     @SWG\Response(
    *          response=200,
    *          description="Ad details",
    *      ),
    *     @SWG\Response(
    *          response="default",
    *          description="error"
    *   )
    * ),
    */

    public function getSingleAdBySlug(Request $request){
        if($request->type == 'event'){
            $item = \App\Models\Post::with(['postfile', 'tags','files','categories','createdby'])
            ->where(['slug'=> $request->slug])->first();
        }
        elseif($request->type == 'hotel'){
            $item = \App\Models\Hotel::with(['tags', 'featureImage', 'files','createdby'])
            ->where(['slug'=> $request->slug])->first();
        }
        elseif($request->type == 'restaurant'){
            $item = \App\Models\Restaurant::with(['featureImage','tags', 'files','createdby'])
            ->where(['slug'=> $request->slug])->first();
        }
        
        return response()->json(['item'=>$item, 'metas'=>['version'=> env('APP_VERSION')]], 200);

    }
}