<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller as BaseController;
/**
 * @SWG\Swagger(
 *	   basePath="/api",
 *	   schemes=API_SCHEMES,
 *     host=API_HOST,
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *          @SWG\Info(
 *              title="Weekend Tootle Api docs",
 *              version="1.0.0",
 *              description="Swagger creates human-readable documentation for your APIs.",
 *              @SWG\Contact(name="JP Caparas",email="jp@pixelfusion.co.nz"),
 *              @SWG\License(name="Unlicense")
 *          )
 * )
 */
class Controller extends BaseController
{
    //
}