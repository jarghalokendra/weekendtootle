<?php

namespace App\Http\Controllers\Api;
use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Http\Request;


class FeatureAdController extends Controller
{

    /**
    * @SWG\Get(
    *     path="/v1/data/feature-ad",
    *     tags={"FeatureAd"},
    *     summary="Feature ad are listed here",
    *     @SWG\Response(
    *          response=200,
    *          description="Features Ad Data",
    *      ),
    *     @SWG\Response(
    *          response="default",
    *          description="error"
    *   )
    * ),
    */

    public function getFeatureAd(){
    	$sliders = \App\Models\Slider::where(['status'=>1, 'type'=>'feature'])
        ->with(['post'=>function($q){
            return $q->with('postfile');
        }])
        ->orderBy('cost','desc')
        ->get();
        return response()->json(['features'=>$sliders, 'metas'=> ['app_version'=> env('APP_VERSION')]], 200);
    }
}