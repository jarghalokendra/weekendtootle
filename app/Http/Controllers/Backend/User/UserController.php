<?php

namespace App\Http\Controllers\Backend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\CommonServices;
use App\Http\Requests\UserRequest;
class UserController extends Controller
{
    public function __construct(CommonServices $commonservice){
        $this->middleware(getcurrentroles('users'));
        $this->commonservice = $commonservice;
    }
    public function store(UserRequest $request){
        $user=new User([
            'first_name' => $request->first_name,
            'last_name'=>$request->last_name,
            'email' => $request->email,
            'password'=>bcrypt($request->password),
            'ip_address' =>$request->ip(),
            'phone_no'=> $request->phone_no,
            'mobile_no'=> $request->mobile_no,
            'address'=> $request->address,
            'profile_image'=> $request->has('profile_image') ? $request->profile_image['id'] : NULL
        ]);
        $user->save();
        if($request->has('roles')){
            foreach($request->roles as $role){
                $ids[] = $role['id'];
            }
            $user->roles()->sync($ids);
        }
        
        return response()->json(['message'=>'successfully user created'],200);
    }

    public function index(Request $request){
        $page = $request->perPage;
        $search = $request->search;
        $column = $request->has('column') ? $request->column : 'created_at';
        $order = $request->has('order') ? $request->order : 'desc';
        $data=User::with(['roles']);
        if($request->has('search')){
            $data = $data->where('first_name','like',"$search%")
            ->orWhere('last_name','like',"$search%")
            ->orWhere('email','like',"$search%")
            ->orWhereHas('roles', function ($q) use ($search) {
                return $q->where('name', 'like', "$search%")
                ->orWhere('description', 'like', "$search%");
            });
        }
        $data = $data->orderBy($column,$order)
        ->paginate($page);
        return response()->json(['data'=>$data,'message'=>'Successfully data fetch.'],200); 
    }
    public function edit($id){
        $user=User::with(['roles','profileimage'])->find($id);
        return response()->json(['edit'=>$user],200);
    }
    public function update($id,UserRequest $request){
        $user = User::find($id);
        $data=[
            'first_name' => $request->first_name,
            'last_name'=>$request->last_name,
            'email' => $request->email,
            'phone_no'=> $request->phone_no,
            'mobile_no'=> $request->mobile_no,
            'address'=> $request->address,
            'profile_image'=> $request->has('profile_image') && !empty($request->profile_image['id']) ? $request->profile_image['id']: $user->profile_image
        ];
        $user->update($data);
        if($request->has('roles')){
            foreach($request->roles as $role){
                $ids[] = $role['id'];
            }
            $user->roles()->sync($ids);
        }
        return response()->json(['message'=>'successfully updated.'],200);
    }

    public function status(Request $request){
        $model = User::find($request->id);
        $model->status = $request->status ? 1 : 0;
        $model->save();
        return response()->json(['message'=>'Successfully update status.'],200);
    }

    public function delete(Request $request){
        $id = $request->all();
        $country=User::whereIn('id',$id);
        $country->delete();
        return response()->json(['message'=>'Successfully deleted'],200);  
    }
}
