<?php

namespace App\Http\Controllers\Backend\Restaurant;

use App\Http\Controllers\Controller;
use App\Services\CommonServices;
use App\Services\NotificationServices;
use Illuminate\Http\Request;
use App\Http\Requests\RestaurantRequest;
use Illuminate\Support\Facades\Storage;

class RestaurantController extends Controller
{
    
    public function __construct(CommonServices $services, NotificationServices $notify)
    {
        $this->middleware(getcurrentroles('restaurants'));
        $this->services = $services;
        $this->notify = $notify;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $page = $request->perPage;
        $search = $request->search;
        $column = $request->has('column') ? $request->column : 'created_at';
        $order = $request->has('order') ? $request->order : 'desc';
        $data = \App\Models\Restaurant::with(['createdby', 'updatedby', 'featureImage', 'tags'])
        ->where(['post_id'=>$id]);
        if(!$this->services->isAdmin()){
            $data = $data->where(['created_by'=>auth()->user()->id]);
        }
        if ($request->search) {
            $search = $request->search;
            $data = $data->where(function($query) use($search){
                $query->search($search)
                ->orWhereHas('createdby', function ($q) use ($search) {
                    return $q->where('first_name', 'like', "$search%")
                        ->orWhere('last_name', 'like', "$search%")
                        ->orWhere('email', 'like', "$search%");
                })
                ->orWhereHas('tags', function ($q) use ($search) {
                    return $q->where('name', 'like', "$search%")
                        ->orWhere('slug', 'like', "$search%");
                })
                ->orWhereHas('updatedby', function ($q) use ($search) {
                    return $q->where('first_name', 'like', "$search%")
                        ->orWhere('last_name', 'like', "$search%")
                        ->orWhere('email', 'like', "$search%");
                });
            });
        }
        $data = $data->orderBy($column, $order)
            ->paginate($page);
        return response()->json(['data' => $data, 'message' => 'Successfully data fetch.'], 200);
    }

    public function status(Request $request)
    {
        $model = \App\Models\Restaurant::find($request->id);
        $model->status = $request->status ? 1 : 0;
        $model->updated_by = auth()->user()->id;
        $model->save();
        return response()->json(['message' => 'Successfully update status.'], 200);
    }

    public function delete(Request $request)
    {
        $ids = $request->all();
        $files = \App\Models\Restaurant::whereIn('id', $ids);
        $files->delete();
        return response()->json(['message' => 'Successfully deleted'], 200);
    }

    public function store(RestaurantRequest $request)
    {
        $post = new \App\Models\Restaurant([
            'name'=> $request->name,
            'content'=> $request->content,
            'created_by'=> auth()->user()->id,
            'post_id' => $request->post_id,
            'feature_image'=> !empty($request->feature_image) ? $request->feature_image['id']: NULL,
            'price'=> $request->price
        ]);
        $post->save();
        if($request->has('restaurant_images') && !empty($request->restaurant_images)){
            $ids = $this->services->getIdsArray($request->restaurant_images);
            $post->files()->sync($ids);
        }
        if($request->has('tags')){
            $ids = $this->services->getTagsId($request->tags);
            $post->tags()->sync($ids);
        }
        return response()->json(['message'=>'successfully created.'],200);
    }

    public function edit($id)
    {
        $data = \App\Models\Restaurant::with(['featureImage','files','tags'])->find($id);
        return response()->json(['message' => 'Fetch data successfully', 'edit' => $data], 200);
    }

    public function update(RestaurantRequest $request, $id)
    {
        $post = \App\Models\Restaurant::find($id);
        $data = [
            'name'=> $request->name,
            'content'=> $request->content,
            'updated_by'=> auth()->user()->id,
            'post_id'=> $request->post_id,
            'feature_image'=> !empty($request->feature_image) ? $request->feature_image['id']: NULL,
            'price'=> $request->price
        ];
        $post->update($data);
        if($request->has('restaurant_images')){
            $ids = $this->services->getIdsArray($request->restaurant_images);
            $post->files()->sync($ids);
        }
        if($request->has('tags')){
            $ids = $this->services->getTagsId($request->tags);
            $post->tags()->sync($ids);
        }
        return response()->json(['message'=>'successfully update.'],200);
    }

}