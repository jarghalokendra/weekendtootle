<?php

namespace App\Http\Controllers\Backend\Sliders;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\CommonServices;
use App\Http\Requests\SliderRequest;
class SliderController extends Controller
{
    public function __construct(CommonServices $commonservice){
        $this->middleware(getcurrentroles('users'));
        $this->commonservice = $commonservice;
    }
    public function store(SliderRequest $request){
        $slider=new \App\Models\Slider([
            'post_id' => $request->post['id'],
            'cost' => $request->cost,
            'type'=> $request->type['name'],
            'created_by' =>auth()->user()->id,
            'expiry_at'=> $request->expiry_at,
            'start_at'=> $request->start_at
        ]);
        $slider->save();
        return response()->json(['message'=>'successfully sliders created'],200);
    }

    public function index(Request $request){
        $page = $request->perPage;
        $search = $request->search;
        $column = $request->has('column') ? $request->column : 'created_at';
        $order = $request->has('order') ? $request->order : 'desc';
        $data=\App\Models\Slider::with(['post']);
        if($request->has('search')){
            $data = $data->where('cost','like',"$search%")
            ->orWhere('type','like',"$search%")
            ->orWhere('start_at','like',"$search%")
            ->orWhere('expiry_at','like',"$search%")
            ->orWhere('created_at','like',"$search%")
            ->orWhere('id','like',"$search%")
            ->orWhereHas('post',function($q) use($search){
                return $q->where('post_title','like', "%$search%")
                ->orWhere('slug','like',"$search%");
            });
        }
        $data = $data->orderBy($column,$order)
        ->paginate($page);
        return response()->json(['data'=>$data,'message'=>'Successfully data fetch.'],200); 
    }
    public function edit($id){
        $user=\App\Models\Slider::with(['post'])->find($id);
        return response()->json(['edit'=>$user],200);
    }
    public function update($id,SliderRequest $request){
        $slider = \App\Models\Slider::find($id);
        $data=[
            'post_id' => $request->post['id'],
            'cost' => $request->cost,
            'type'=> $request->type['name'],
            'updated_by' =>auth()->user()->id,
            'expiry_at'=> $request->expiry_at,
            'start_at'=> $request->start_at
        ];
        $slider->update($data);
        return response()->json(['message'=>'successfully updated.'],200);
    }

    public function status(Request $request){
        $model = \App\Models\Slider::find($request->id);
        $model->status = $request->status ? 1 : 0;
        $model->save();
        return response()->json(['message'=>'Successfully update status.'],200);
    }

    public function delete(Request $request){
        $id = $request->all();
        $slider=\App\Models\Slider::whereIn('id',$id);
        $slider->delete();
        return response()->json(['message'=>'Successfully deleted'],200);  
    }
}
