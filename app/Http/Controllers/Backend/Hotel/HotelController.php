<?php

namespace App\Http\Controllers\Backend\Hotel;

use App\Http\Controllers\Controller;
use App\Services\CommonServices;
use App\Services\NotificationServices;
use Illuminate\Http\Request;
use App\Http\Requests\HotelRequest;
use Illuminate\Support\Facades\Storage;

class HotelController extends Controller
{
    
    public function __construct(CommonServices $services, NotificationServices $notify)
    {
        $this->middleware(getcurrentroles('hotels'));
        $this->services = $services;
        $this->notify = $notify;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $page = $request->perPage;
        $search = $request->search;
        $column = $request->has('column') ? $request->column : 'created_at';
        $order = $request->has('order') ? $request->order : 'desc';
        $data = \App\Models\Hotel::with(['createdby', 'updatedby', 'featureImage', 'tags'])
        ->where(['post_id'=>$id]);
        if(!$this->services->isAdmin()){
            $data = $data->where(['created_by'=>auth()->user()->id]);
        }
        if ($request->search) {
            $search = $request->search;
            $data = $data->where(function($query) use($search){
                $query->search($search)
                ->orWhereHas('createdby', function ($q) use ($search) {
                    return $q->where('first_name', 'like', "$search%")
                        ->orWhere('last_name', 'like', "$search%")
                        ->orWhere('email', 'like', "$search%");
                })
                ->orWhereHas('tags', function ($q) use ($search) {
                    return $q->where('name', 'like', "$search%")
                        ->orWhere('slug', 'like', "$search%");
                })
                ->orWhereHas('updatedby', function ($q) use ($search) {
                    return $q->where('first_name', 'like', "$search%")
                        ->orWhere('last_name', 'like', "$search%")
                        ->orWhere('email', 'like', "$search%");
                });
            });
        }
        $data = $data->orderBy($column, $order)
            ->paginate($page);
        return response()->json(['data' => $data, 'message' => 'Successfully data fetch.'], 200);
    }

    public function status(Request $request)
    {
        $model = \App\Models\Hotel::find($request->id);
        $model->status = $request->status ? 1 : 0;
        $model->updated_by = auth()->user()->id;
        $model->save();
        return response()->json(['message' => 'Successfully update status.'], 200);
    }

    public function delete(Request $request)
    {
        $ids = $request->all();
        $files = \App\Models\Hotel::whereIn('id', $ids);
        $files->delete();
        return response()->json(['message' => 'Successfully deleted'], 200);
    }

    public function store(HotelRequest $request)
    {
        $post = new \App\Models\Hotel([
            'room_no'=> $request->room_no,
            'room_type'=> $request->room_type,
            'content'=> $request->content,
            'created_by'=> auth()->user()->id,
            'post_id' => $request->post_id,
            'feature_image'=> !empty($request->feature_image) ? $request->feature_image['id']: NULL,
            'price'=> $request->price
        ]);
        $post->save();
        if($request->has('hotel_images') && !empty($request->hotel_images)){
            $ids = $this->services->getIdsArray($request->hotel_images);
            $post->files()->sync($ids);
        }
        if($request->has('tags')){
            $ids = $this->services->getTagsId($request->tags);
            $post->tags()->sync($ids);
        }
        return response()->json(['message'=>'successfully created.'],200);
    }

    public function edit($id)
    {
        $data = \App\Models\Hotel::with(['featureImage','files','tags'])->find($id);
        return response()->json(['message' => 'Fetch data successfully', 'edit' => $data], 200);
    }

    public function update(HotelRequest $request, $id)
    {
        $post = \App\Models\Hotel::find($id);
        $data = [
            'room_no'=> $request->room_no,
            'room_type'=> $request->room_type,
            'content'=> $request->content,
            'updated_by'=> auth()->user()->id,
            'post_id'=> $request->post_id,
            'feature_image'=> !empty($request->feature_image) ? $request->feature_image['id']: NULL,
            'price'=> $request->price
        ];
        $post->update($data);
        if($request->has('hotel_images')){
            $ids = $this->services->getIdsArray($request->hotel_images);
            $post->files()->sync($ids);
        }
        if($request->has('tags')){
            $ids = $this->services->getTagsId($request->tags);
            $post->tags()->sync($ids);
        }
        return response()->json(['message'=>'successfully update.'],200);
    }

}