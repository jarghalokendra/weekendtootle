<?php

namespace App\Http\Controllers\Backend\Post;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\CommonServices;
use App\Models\Post;
use App\Http\Requests\EventRequest;

class EventController extends Controller
{
    public function __construct(CommonServices $services)
    {
        $this->middleware('role:admin|vendor');
        $this->services = $services;
    }

    public function update(EventRequest $request, $id){
        $event = Post::find($id);
        $data = [
            'start_date'=> $request->start_date,
            'end_date'=> $request->end_date,
            'event_status'=> $request->status ? 1: 0
        ];
        $event->update($data);
        return response()->json(['message'=>'Successfully saved!'],200);
    }
   
}
