<?php

namespace App\Http\Controllers\Backend\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Services\CommonServices;
use App\Services\NotificationServices;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    
    public function __construct(CommonServices $services, NotificationServices $notify)
    {
        $this->middleware(getcurrentroles('posts'));
        $this->services = $services;
        $this->notify = $notify;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->perPage;
        $search = $request->search;
        $column = $request->has('column') ? $request->column : 'created_at';
        $order = $request->has('order') ? $request->order : 'desc';
        $data = Post::with(['categories', 'files', 'postfile', 'reach', 'createdby', 'updatedby']);
        if(!$this->services->isAdmin()){
            $data = $data->where(['user_id'=>auth()->user()->id]);
        }
        if($request->type){
            $data = $data->where(['post_type'=>$request->type]);
            if($request->type == 'restaurant'){
                $data = $data->withCount('restaurant');
            }
            elseif($request->type == 'hotel'){
                $data = $data->withCount('hotel');
            }
        }
        if ($request->search) {
            $search = $request->search;
            $data = $data->where(function($query) use($search){
                $query->search($search)
                ->orWhereHas('categories', function ($q) use ($search) {
                    return $q->where('name', 'like', "$search%")
                        ->orWhere('slug', 'like', "$search%");
                })
                ->orWhereHas('createdby', function ($q) use ($search) {
                    return $q->where('first_name', 'like', "$search%")
                        ->orWhere('last_name', 'like', "$search%")
                        ->orWhere('email', 'like', "$search%");
                })
                ->orWhereHas('updatedby', function ($q) use ($search) {
                    return $q->where('first_name', 'like', "$search%")
                        ->orWhere('last_name', 'like', "$search%")
                        ->orWhere('email', 'like', "$search%");
                });
            });
        }
        $data = $data->orderBy($column, $order)
            ->paginate($page);
        return response()->json(['data' => $data, 'message' => 'Successfully data fetch.'], 200);
    }

    public function status(Request $request)
    {
        $model = Post::find($request->id);
        if($request->has('type') && $request->type == 'event'){
            $model->event_status = $request->status ? 1 : 0;
        }
        else{
            $model->post_status = $request->status ? 1 : 0;
        }
        $model->save();
        return response()->json(['message' => 'Successfully update status.'], 200);
    }

    public function delete(Request $request)
    {
        $ids = $request->all();
        $files = Post::whereIn('id', $ids);
        $files->delete();
        return response()->json(['message' => 'Successfully deleted'], 200);
    }

    public function store(PostRequest $request)
    {
        $post = new Post([
            'post_title'=> $request->post_title,
            'post_excerpt'=> $request->post_excerpt,
            'post_content'=> $request->post_content,
            'post_type'=> $request->post_type['value'],
            'user_id'=> auth()->user()->id,
            'feature_image'=> !empty($request->feature_image) ? $request->feature_image['id']: NULL,
            'start_date'=> $request->start_date,
            'end_date'=> $request->end_date,
            'latitude'=> $request->location['latitude'],
            'longitude'=> $request->location['longitude'],
            'address' => $request->location['address'],
            'location_name' => $request->location['location_name'],
            'vicinity' => $request->location['vicinity'],
            'city' => $request->location['city'],
            'zone' => $request->location['zone'],
            'state' => $request->location['state'],
            'postal_code' => $request->location['postal_code'],
            'country' => $request->location['country'],
            'email' => $request->email,
            'phone' => $request->phone
        ]);
        $post->save();
        if($request->has('categories')){
            $ids=$this->services->getIdsArray($request->categories);
            $post->categories()->sync($ids);
        }
        if($request->has('post_images')){
            $ids = $this->services->getIdsArray($request->post_images);
            $post->files()->sync($ids);
        }
        if($request->has('tags')){
            $ids = $this->services->getTagsId($request->tags);
            $post->tags()->sync($ids);
        }
        $pushNotifyKey = null;
        if($this->services->isVendor() && !$this->services->isAdmin()){
            $data = [
                'module_name' => $post->post_title,
                'module_id'=> $post->id,
                'message' => auth()->user()->first_name.' was created '.$post->title,
                'from'=> auth()->user()->id,
                'roles'=> ['admin']
            ];
            $this->notify->saveNotifications($data);
            $pushNotifyKey = $this->notify->pushNotifications($data);

        }
        return response()->json(['message'=>'successfully created.', 'notify'=> $pushNotifyKey],200);
    }

    public function edit($id)
    {
        $data = Post::with(['postfile','categories','files','postfile','tags'])->find($id);
        $data['location'] = [
            'address'=> $data->address,
            'location_name'=> $data->location_name,
            'city'=> $data->city,
            'zone'=> $data->zone,
            'postal_code'=> $data->postal_code,
            'state'=> $data->state,
            'vicinity'=> $data->vicinity,
            'longitude'=> $data->longitude,
            'latitude'=> $data->latitude,
            'country'=> $data->country,
        ];
        return response()->json(['message' => 'Fetch data successfully', 'edit' => $data], 200);
    }

    public function update(PostRequest $request, $id)
    {
        $post = Post::find($id);
        $data = [
            'post_title'=> $request->post_title,
            'post_excerpt'=> $request->post_excerpt,
            'post_content'=> $request->post_content,
            'post_type'=> $request->post_type['value'],
            'updatedby'=> auth()->user()->id,
            'feature_image'=> !empty($request->feature_image) ? $request->feature_image['id']: NULL,
            'start_date'=> $request->start_date,
            'end_date'=> $request->end_date,
            'address' => $request->location['address'],
            'latitude'=> $request->location['latitude'],
            'longitude'=> $request->location['longitude'],
            'location_name' => $request->location['location_name'],
            'vicinity' => $request->location['vicinity'],
            'city' => $request->location['city'],
            'zone' => $request->location['zone'],
            'state' => $request->location['state'],
            'postal_code' => $request->location['postal_code'],
            'country' => $request->location['country'],
            'email' => $request->email,
            'phone' => $request->phone
        ];
        $post->update($data);
        if($request->has('categories')){
            $ids= $this->services->getIdsArray($request->categories);
            $post->categories()->sync($ids);
        }
        if($request->has('post_images')){
            $ids= $this->services->getIdsArray($request->post_images);
            $post->files()->sync($ids);
        }
        if($request->has('tags')){
            $ids = $this->services->getTagsId($request->tags);
            $post->tags()->sync($ids);
        }
        return response()->json(['message'=>'successfully update.'],200);
    }

}