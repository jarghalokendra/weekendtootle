<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Services\CommonServices;
use App\Http\Requests\UserRequest;

class CommonController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    |
    */
    public function __construct(CommonServices $services)
    {
        //$this->middleware('role:admin|vendor');
        $this->services = $services;
    }

    public function menus(){
        $menus = $this->services->getMenuByRoles();
        return response()->json(['menus'=>$menus],200);
    }

    public function categories(){
        return response()->json(['categories'=>$this->services->categories()],200);
    }

    public function tags(){
        return response()->json(['tags'=>$this->services->tags()],200);
    }

    public function roles(){
        return response()->json(['roles'=>$this->services->roles()],200);
    }

    public function posts(){
        return response()->json(['posts'=>$this->services->posts()],200);
    }

    public function getCommonModules(){
        return response()->json(['commonmodules'=>$this->services->getCommonModule()],200);
    }

    public function getProfile(){
        return response()->json(['profile'=> $this->services->getProfile()],200);
    }

    public function getPlaces(Request $request){
        // https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input="+name+"&inputtype=textquery&fields=formatted_address, geometry, icon, name, permanently_closed, photo, place_id, plus_code, type, vicinity&key=AIzaSyAhSv9zWvisiTXRPRw6K8AE0DCmrRMpQcU

        // $client = new \GuzzleHttp\Client();
        // $response = $client->request('POST', 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json', [
        //     'headers' => [
        //         'apikey' => 'AIzaSyAhSv9zWvisiTXRPRw6K8AE0DCmrRMpQcU',
        //         'Content-Type' => 'application/json',
        //     ],
        //     'form_params' => [
        //         "input" => $request->name,
        //         "inputtype"  => "textquery",
        //         "fields"=> "formatted_address, geometry, icon, name, permanently_closed, photo, place_id, plus_code",
        //         //"key"=>"AIzaSyAhSv9zWvisiTXRPRw6K8AE0DCmrRMpQcU"
        //     ]
        // ]);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=".$request->name."&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=AIzaSyAhSv9zWvisiTXRPRw6K8AE0DCmrRMpQcU");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        $output;
        // $response = $response->getBody()->getContents();
        // $response = json_decode($response);
        return response()->json(['places'=> $output], 200);
    }

    public function updateProfile(UserRequest $request){
        $user = \App\Models\User::find(auth()->user()->id);
        $data=[
            'first_name' => $request->first_name,
            'last_name'=>$request->last_name,
            'phone_no'=> $request->phone_no,
            'mobile_no'=> $request->mobile_no,
            'address'=> $request->address,
            'profile_image'=> $request->has('profileimage') && !empty($request->profileimage['id']) ? $request->profileimage['id']: $user->profile_image
        ];
        if($request->password_status == true){
            $data['password'] = bcrypt($request->password);
        }
        $user->update($data);
        return response()->json(['profile'=> $this->services->getProfile(), 'message'=>'successfully profile updated.'],200);
    }
   
}
