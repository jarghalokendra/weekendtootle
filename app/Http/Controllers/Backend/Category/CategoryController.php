<?php

namespace App\Http\Controllers\Backend\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
class CategoryController extends Controller
{
    public function __construct(){
        $this->middleware(getcurrentroles('categories'));
    }

    public function store(CategoryRequest $request){
        $category = new Category([
            'name' => $request->name,
            'description' => $request->description
        ]);
        $category->save();
        return response()->json(['message'=>'successfully added'],200);
    }
  

    public function index(Request $request){
        $page = $request->perPage;
        $search = $request->search;
        $column = $request->has('column') ? $request->column : 'created_at';
        $order = $request->has('order') ? $request->order : 'desc';
        $data=Category::query();
        if($request->has('search')){
            $data = $data->where('name','like',"$search%")
            ->orWhere('slug','like',"$search%")
            ->orWhere('created_at','like',"$search%")
            ->orWhere('description','like',"$search%");
        }
        $data = $data->orderBy($column,$order)
        ->paginate($page);
        return response()->json(['data'=>$data,'message'=>'Successfully data fetch.'],200); 
    }

    public function edit($id)
    {
        $role = Category::find($id);
        return response()->json(['data'=>$role],200);
    }

    public function update($id, Request $request)
    {
        $role = Category::find($id);
        $role->update($request->all());
        return response()->json(['message'=>'successfully updated'],200);
    }

    public function delete(Request $request){
        $id = $request->all();
        $role=Category::whereIn('id',$id);
        $role->delete();
        return response()->json(['message'=>'Successfully deleted'],200);  
    }

    public function status(Request $request){
        $model = Category::find($request->id);
        $model->status = $request->status ? 1 : 0;
        $model->save();
        return response()->json(['message'=>'Status change successfully.'],200);
    }
}
