<?php

namespace App\Http\Controllers\Backend\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Models\Role;
class RoleController extends Controller
{

    public function __construct(){
        $this->middleware(getcurrentroles('roles'));
    }
    
    public function store(RoleRequest $request){
        $role=new Role([
            'name'=>$request->get('name'),
            'description'=>$request->get('description')
        ]);
        $role->save();
        return response()->json('successfully added');
    }
  

    public function index(Request $request){
        $page = $request->perPage;
        $search = $request->search;
        $column = $request->has('column') ? $request->column : 'created_at';
        $order = $request->has('order') ? $request->order : 'desc';
        $data=Role::query();
        if($request->has('search')){
            $data = $data->where('name','like',"$search%")
            ->orWhere('description','like',"$search%");
        }
        $data = $data->orderBy($column,$order)
        ->paginate($page);
        return response()->json(['data'=>$data,'message'=>'Successfully data fetch.'],200); 
    }

    public function edit($id)
        {
        $role = Role::find($id);
        return response()->json($role);
        }

    public function update($id, Request $request)
    {
        $role = Role::find($id);
        $role->update($request->all());
        //return $post;
        return response()->json(['message'=>'successfully updated'],200);
    }
    public function getAllRole(){
        $role=Role::all();
        return  $role;
    }
    public function delete(Request $request){
        $id = $request->all();
        $role=Role::whereIn('id',$id);
        $role->delete();
        return response()->json(['message'=>'Successfully deleted'],200);  
    }

    public function status(Request $request){
        $model = Role::find($request->id);
        $model->status = $request->status ? 1 : 0;
        $model->save();
        return response()->json(['message'=>'Status change successfully.'],200);
    }

     
}
