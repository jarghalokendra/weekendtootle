<?php

namespace App\Http\Controllers\Backend\Media;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\File;

class MediaController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    |
    */
    public function __construct()
    {
        $this->middleware('role:admin|vendor');
    }

    public function index(Request $request){
        $data = File::where(['user_id'=>auth()->user()->id])
        ->orderBy('created_at','desc')
        ->get();
        return response()->json(['data'=>$data,'message'=>'Successfully data fetch.'],200);
    }


    public function upload(Request $request){
        $data = $request->all();
        $validator = Validator::make(
        $data, [
            'data.*' => 'required|file|mimes:jpg,jpeg,png,gif,doc,docx,pdf',
        ],
        [
            'data.*.required' => 'Please upload an image',
            'data.*.mimes' => 'Only jpg,jpeg,png,gif,doc,docx,pdf are allowed',

        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 422, 'message' => $validator->errors()->all()],200);
        }
        else{
            if($request->has('type') && $request->type == 'editor'){
                $model = new File;
                $img = $request->file('image');
                $original_name = $img->getClientOriginalName();
                $save_name = time().'-'.$img->getClientOriginalName();
                $file_type = $img->getClientOriginalExtension();
                $img->move(public_path('images/uploads'),$save_name);
                $data = [
                    'original_name'=>$original_name,
                    'save_name' => url('images/uploads/'.$save_name),
                    'file_type' => $file_type,
                    'user_id' => auth()->user()->id
                ];
                $model->create($data);
                return response()->json(['url'=>url('images/uploads/'.$save_name)],200);
            }
            foreach ($request->file('file') as $key => $val) {
                $model = new File;
                $original_name = $val->getClientOriginalName();
                $save_name = time().'-'.$val->getClientOriginalName();
                $file_type = $val->getClientOriginalExtension();
                $val->move(public_path('images/uploads'),$save_name);
                $data = [
                    'original_name'=>$original_name,
                    'save_name' => url('images/uploads/'.$save_name),
                    'file_type' => $file_type,
                    'user_id' => auth()->user()->id
                ];
                $model->create($data);
            }
            return response()->json(['message'=>'Successfully uploaded.'],200);
        }
    }

    public function delete(Request $request){
        $ids = $request->all();
        $files = File::whereIn('id',$ids);
        foreach ($files->get() as $val) {
            $f = explode('public', $val->save_name);
            if(file_exists(public_path($f[1])))
                unlink(public_path($f[1]));
        }
        $files->delete();
        return response()->json(['message'=>'Successfully deleted'],200);  
    }

    
}
