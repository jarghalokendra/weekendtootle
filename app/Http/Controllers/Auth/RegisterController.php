<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Services\CommonServices;
use Validator;
use App\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Rgister Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles register users for the application
    |
    */

    /**
    *   @SWG\post(
    *       path="/auth/register",
    *       tags={"Auth"},
    *       summary="Register",
    *       @SWG\Parameter(
    *           name="first_name",
    *           in="query",
    *           schema={},
    *           required=true,
    *           type="string",
    *           description="First name",
    *       ),
    *       @SWG\Parameter(
    *           name="last_name",
    *           in="query",
    *           schema={},
    *           required=false,
    *           type="string",
    *           description="Last name",
    *       ),
    *       @SWG\Parameter(
    *           name="email",
    *           in="query",
    *           schema={},
    *           required=true,
    *           type="string",
    *           description="Email(email must be unique and email is used as username for this app.)",
    *       ),
    *       @SWG\Parameter(
    *           name="password",
    *           in="query",
    *           schema={},
    *           required=true,
    *           type="string",
    *           description="Password"
    *       ),
    *       @SWG\Parameter(
    *           name="password_confirmation",
    *           in="query",
    *           schema={},
    *           required=true,
    *           type="string",
    *           description="Confirm password (password must be same.)"
    *       ),
    *       @SWG\Response(
    *          response=200,
    *          description="Register"
    *       ),
    *       @SWG\Response(
    *           response="default",
    *           description="error"
    *       )
    *   )
    */
    public function __construct(CommonServices $commonServices)
    {
        $this->middleware('auth:api', ['except' => ['register']]);
        $this->commonServices = $commonServices;
    }

    public function register(RegisterRequest $request){
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $data['ip_address'] = $request->ip();
        $user=User::create($data);
        $role_id = $this->commonServices->getDefaultRole();
        $user->roles()->sync([$role_id]);
        return response()->json(['message'=>'You are successfully register now!'],200);  
    }
}