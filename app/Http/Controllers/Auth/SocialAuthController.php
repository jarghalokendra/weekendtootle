<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use App\Services\CommonServices;
use Socialite;
use Request;


class SocialAuthController extends Controller
{

    public function __construct(CommonServices $commonServices){
        $this->commonServices = $commonServices;
    }

    public function providerRedirect($provider = ''){

        if(!in_array($provider,['facebook','google']))
            return redirect('/login')->withErrors('This is not a valid link.');

        return Socialite::driver($provider)->redirect();
    }

    public function providerRedirectCallback($provider = '')
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return redirect('/auth/social');
        }
        $userExist = \App\Models\User::whereEmail($user->email)->first();
        if($userExist){
            if($userExist->status == 2)
                return response()->json(['message' => ['invalid'=>['Your account hasn\'t been activated. Please check your email & activate account.']]], 422);
            if($userExist->status == 0)
                return response()->json(['message' => ['invalid'=>['Your account is banned. Please contact system administrator.']]], 422);
            if($userExist->status != 1)
                return response()->json(['message' => ['invalid'=>['There is something wrong with your account. Please contact system administrator.']]], 422);
            $token = auth()->login($userExist);
        }
        else {
            $new_user = new \App\Models\User;
            $new_user->email = $user->email;
            $new_user->provider = $provider;
            $new_user->provider_unique_id = $user->id;
            $new_user->status = 1;
            $new_user->ip_address = Request::ip();
            $name = explode(' ',$user->name);
            $new_user->first_name = array_key_exists(0, $name) ? $name[0] : 'John';
            $new_user->last_name = array_key_exists(1, $name) ? $name[1] : 'Doe';
            $new_user->save();
            $role_id = $this->commonServices->getDefaultRole();
            $new_user->roles()->sync([$role_id]);
            $token = auth()->login($new_user);
        }

        \Cache::put('access_token', $token, 1);
        return redirect('/#/auth/social');
    }

    public function getSocialToken(){
        if(!\Cache::has('access_token'))
            return response()->json(['message' => ['invalid'=>['Invalid request.']]],422);

        $token = \Cache::get('access_token');
        \Cache::forget('access_token');
        return response()->json(
            [
                'message' => 'You are successfully logged in!', 
                'access_token' => $token
            ]
        );
    }
}
