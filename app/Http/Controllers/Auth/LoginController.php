<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Services\CommonServices;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    |
    */

    /**
    *   @SWG\post(
    *       path="/auth/login",
    *       tags={"Auth"},
    *       summary="Login",
    *       @SWG\Parameter(
    *           name="email",
    *           in="query",
    *           schema={},
    *           required=true,
    *           type="string",
    *           description="Email",
    *       ),
    *       @SWG\Parameter(
    *           name="password",
    *           in="query",
    *           schema={},
    *           required=true,
    *           type="string",
    *           description="Password"

    *       ),
    *       @SWG\Response(
    *          response=200,
    *          description="Login"
    *       ),
    *       @SWG\Response(
    *           response="default",
    *           description="error"
    *       )
    *   ),
    *   @SWG\post(
    *       path="/auth/logout",
    *       tags={"Auth"},
    *       summary="LogOut",
    *       @SWG\Response(
    *          response=200,
    *          description="Logout"
    *       ),
    *       @SWG\Response(
    *           response="default",
    *           description="error"
    *       )
    *   ),
    */
    public function __construct(CommonServices $commonServices)
    {
        $this->middleware('auth:api', ['except' => ['authenticate','check']]);
        $this->commonServices = $commonServices;
    }

    public function authenticate(Request $request){
        $validation = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
            'password' => 'required'
        ]);

        if($validation->fails())
            return response()->json(['message' => $validation->messages()],422);
        $credentials = $request->only('email', 'password');
        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['message' => ['invalid'=>['Invalid Credentials! Please try again.']]], 422);
        }

        $user = User::whereEmail(request('email'))->first();
        if($user->status == 2)
            return response()->json(['message' => ['invalid'=>['Your account hasn\'t been activated. Please check your email & activate account.']]], 422);
        if($user->status == 0)
            return response()->json(['message' => ['invalid'=>['Your account is banned. Please contact system administrator.']]], 422);
        if($user->status != 1)
            return response()->json(['message' => ['invalid'=>['There is something wrong with your account. Please contact system administrator.']]], 422);
        return $this->respondWithToken($token);
    }

    public function social(Request $request){
        return $request->all();
    }

    public function check(){
        if(!empty(auth()->user())){
            return response()->json(['authenticated'=>true],200);
        }
        return response()->json(['authenticated'=>false],401);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'commonmodules'=> $this->commonServices->getCommonModule(),
            'message' => 'Login successfully.'
        ]);
    }
}