<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Services\CommonServices;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function __construct(CommonServices $service)
    {
        $this->service = $service;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $requiestroles = explode('|', $guard);
        $existroles = $this->validateRoles();
        foreach ($requiestroles as $role) {
            if(in_array($role, $existroles)){
                return $next($request);
            }
        }
        return response()->json(['status'=>'unauthorised','message'=>'unauthorised'],401);
    }

    function validateRoles(){
        return $this->service->getRoles();
    }
}
