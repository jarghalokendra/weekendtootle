<?php

namespace App\Models;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'post_title'
            ]
        ];
    }

    protected $table = 'posts';

    protected $fillable = [
        'post_title', 'post_content', 'post_excerpt', 'user_id', 'slug', 'feature_image', 'post_type', 
        'updated_by', 'longitude', 'latitude','start_date', 'event_status','end_date','address', 
        'location_name', 'city', 'zone', 'state','vicinity', 'postal_code', 'country', 'email', 'phone'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_post', 'post_id', 'category_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'tag_post', 'post_id', 'tag_id');
    }

    public function restaurant()
    {
        return $this->hasMany(Restaurant::class, 'post_id', 'id');
    }

    public function hotel()
    {
        return $this->hasMany(Hotel::class, 'post_id', 'id');
    }

    public function files()
    {
        return $this->belongsToMany(File::class, 'post_file', 'post_id', 'file_id');
    }

    public function postfile()
    {
        return $this->belongsTo(File::class, 'feature_image', 'id');
    }
    public function reach()
    {
        return $this->belongsTo(PostReach::class);
    }

    public function createdby()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function updatedby()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class, 'id', 'post_id');
    }

    public function scopeSearch($query, $name)
    {
        if ($name) {
            return $query->where('post_title', 'like', "$name%")
                ->orWhere('post_excerpt', 'like', "$name%")
                ->orWhere('slug', 'like', "$name%")
                ->orWhere('post_type', 'like', "$name%")
                ->orWhere('latitude', 'like', "$name%")
                ->orWhere('longitude', 'like', "$name%")
                ->orWhere('post_content', 'like', "$name%")
                ->orWhere('post_status', 'like', "$name%")
                ->orWhere('address', 'like', "$name%")
                ->orWhere('location_name', 'like', "$name%")
                ->orWhere('vicinity', 'like', "$name%")
                ->orWhere('city', 'like', "$name%")
                ->orWhere('state', 'like', "$name%")
                ->orWhere('zone', 'like', "$name%")
                ->orWhere('postal_code', 'like', "$name%")
                ->orWhere('country', 'like', "$name%")
                ->orWhere('created_at', 'like', "$name%");
        }
    }

    public function scopeIsWithinMaxDistance($query, $coordinates, $radius = 5) {

        $haversine = "(6371 * acos(cos(radians(" . $coordinates['latitude'] . ")) 
        * cos(radians(`latitude`)) 
        * cos(radians(`longitude`) 
        - radians(" . $coordinates['longitude'] . ")) 
        + sin(radians(" . $coordinates['latitude'] . ")) 
        * sin(radians(`latitude`))))";
        return $query->select('*')
        ->selectRaw("{$haversine} AS distance")
        ->whereRaw("{$haversine} < ?", [$radius])
        ->orderBy('distance', 'asc');
    }

}