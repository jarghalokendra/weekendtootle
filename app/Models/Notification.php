<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    protected $table = 'notifications';

    protected $fillable = ['id','module_name', 'from', 'to', 'message', 'module_id'];


}