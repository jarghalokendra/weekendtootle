<?php

namespace App\Models;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $table = 'restaurants';

    protected $fillable = [
        'name', 'content', 'slug', 'feature_image', 
        'updated_by','price', 'created_by', 'post_id'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'restaurant_tag', 'restaurant_id', 'tag_id');
    }

    public function files()
    {
        return $this->belongsToMany(File::class, 'restaurant_file', 'restaurant_id', 'file_id');
    }

    public function featureImage()
    {
        return $this->belongsTo(File::class, 'feature_image', 'id');
    }

    public function createdby()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updatedby()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function scopeSearch($query, $name)
    {
        if ($name) {
            return $query->where('name', 'like', "$name%")
                ->orWhere('price', 'like', "$name%")
                ->orWhere('slug', 'like', "$name%")
                ->orWhere('status', 'like', "$name%")
                ->orWhere('content', 'like', "$name%")
                ->orWhere('id', 'like', "$name%");
        }
    }

}