<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
	protected $fillable=[
        'post_id', 
        'cost',
        'type',
        'created_by',
        'updated_by',
        'status',
        'expiry_at',
        'start_at',
        'created_at',
        'updated_at'
    ];
	protected $table ="sliders";

	public function post()
	{
	    return $this->belongsTo(Post::class, 'post_id', 'id');
	}

	public function slider(){
		return $this->hasManyThrough(
            'App\Models\Slider',
            'App\Models\File',
            'id', // Foreign key on users table...
            'post_id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'id' // Local key on users table...
        );
	}

}
