<?php

	function getMenus(){
		return menus();
	}

	function menus(){
		$data=[
        [
            'id'=>1,
            'name'=> 'Dashboard',
            'icon'	=> 'fa-home',
            'permissions'=>['admin','vendor'],
            'route'=> 'dashboard'
        ],
        [
            'id'=>2,
            'name'=> 'Roles',
            'icon'  => 'fa-tasks',
            'permissions'=>['admin'],
            'route'=> 'roles'
        ],
        [
            'id'=>3,
            'name'=> 'Categories',
            'icon'	=> 'fa-tags',
            'permissions'=>['admin'],
            'route'=> 'categories'
        ],
        [
            'id'=>4,
            'name'=>'Users',
            'icon' => 'fa-users',
            'permissions'=>['admin'],
            'route'=> 'users'
        ],
        [
            'id'=>11,
            'name'=>'Sliders/ Feature',
            'icon' => 'fa-sliders',
            'permissions'=>['admin'],
            'route'=> 'sliders'
        ],
        [
            'id'=>5,
            'name'=>'Posts',
            'icon'=>'fa-archive',
            'permissions'=>['admin','vendor'],
            'route'=> 'posts'
        ],
        [
            'id'=>6,
            'name'=>'Events',
            'icon'=>'fa-calendar',
            'permissions'=>['admin','vendor'],
            'route'=> "events"
        ],
        [
            'id'=>7,
            'name'=>'Hotels',
            'icon'=>'fa-university',
            'permissions'=>['admin','vendor'],
            'route'=> 'hotels'
        ],
        [
            'id'=>8,
            'name'=>'Restaurants',
            'icon'=>'fa-bars',
            'permissions'=>['admin','vendor'],
            'route'=> 'restaurants'
        ],
        [
            'id'=>9,
            'name'=>'Reviews',
            'icon'=>'fa-star',
            'permissions'=>['admin','vendor'],
            'route'=> 'reviews'
        ],
        [
            'id'=>10,
            'name'=>'Bookings',
            'icon'=>'fa-book',
            'permissions'=>['admin','vendor'],
            'route'=> 'Bookings'
        ]
    ];

		return $data;
	}
    function getmenuitem($url){
        $menus = getMenus();
        foreach ($menus as $menu) {
            if($url == $menu['route']){
                return $menu;
            }
        }
        return false;
    }
    function getcurrentroles($url){
        $menu = getmenuitem($url);
        if($url == $menu['route']){
            if(empty($menu['permissions'])){
                return 'role:'.implode('|', ['admin','vendor']);
            }
            return 'role:'.implode('|', $menu['permissions']);
        }
        return 'role:admin';
    }
    function settings(){
        $data = [
        [
            'id'=>1,
            'name'=>'Roles',
            'icon'=>'fa-check-circle',
            'permissions'=>['admin'],
            'route'=>'roles'
        ]];
        return $data;
    }

